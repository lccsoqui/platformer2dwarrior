extends CharacterBody2D

@export var speed : float = 200.0
@export var jump_velocity : float = -150.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _physics_process(delta):
	# Handle jump
	if not is_on_floor():
		velocity.y += gravity * delta
		if velocity.y > 0:
			$AnimatedSprite2D.play("fall")
		elif velocity.y < 0:
			$AnimatedSprite2D.play("jump_up")

	# Handle run.
	if is_on_floor():
		if Input.is_action_pressed("ui_right"):
			$AnimatedSprite2D.flip_h = false
			$AnimatedSprite2D.play("run")
		elif Input.is_action_pressed("ui_left"):
			$AnimatedSprite2D.flip_h = true
			$AnimatedSprite2D.play("run")
		elif velocity.x == 0:
			$AnimatedSprite2D.play("idel")
			
		if Input.is_action_just_pressed("ui_accept") and is_on_floor():
			velocity.y = jump_velocity	
	
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("ui_left", "ui_right")
	if direction:
		velocity.x = direction * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)

	move_and_slide()
	
